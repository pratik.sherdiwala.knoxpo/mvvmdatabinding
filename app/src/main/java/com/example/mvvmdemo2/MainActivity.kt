package com.example.mvvmdemo2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvvmdemo2.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(
            this,
            R.layout.activity_main
        )
        val activity = this


        with(binding){
            lifecycleOwner = activity
            viewModel = activity.viewModel

            itemsRV.layoutManager = LinearLayoutManager(activity)
            itemsRV.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
            itemsRV.adapter = ItemAdapter()
        }

        with(viewModel) {
            getItems()
        }
    }
}
