package com.example.mvvmdemo2

import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView

object DataBindingAdapter {

    @JvmStatic
    @BindingAdapter("items")
    fun setItems(view: RecyclerView, items: List<String>?) {
        items?.let {
            (view.adapter as? ItemAdapter)?.updateItems(it)
        }
    }
}