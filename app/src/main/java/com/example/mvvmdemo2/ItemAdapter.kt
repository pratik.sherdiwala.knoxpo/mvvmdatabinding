package com.example.mvvmdemo2

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class ItemAdapter : RecyclerView.Adapter<ItemsVH>() {

    private var items: List<String>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsVH {
        return ItemsVH(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_list,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount() = items?.size ?: 0

    override fun onBindViewHolder(holder: ItemsVH, position: Int) {
        holder.bind(items!![position])
    }

    fun updateItems(newItems: List<String>){
        items = newItems
        notifyDataSetChanged()
    }

}