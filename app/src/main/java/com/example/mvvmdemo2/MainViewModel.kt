package com.example.mvvmdemo2

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    val itemList = MutableLiveData<List<String>>()

    fun getItems() {
        val itemList = mutableListOf<String>()
        for (elements in 1..5) {
            itemList.add("Item $elements")
        }
        this.itemList.value = itemList
    }

    fun addItem() {
        val list = itemList.value?.toMutableList() ?: mutableListOf()
        list.add("Item ${list.size}")
        itemList.value = list
    }
}