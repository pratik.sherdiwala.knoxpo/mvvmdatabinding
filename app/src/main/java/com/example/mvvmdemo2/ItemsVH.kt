package com.example.mvvmdemo2

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ItemsVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mItemTV = itemView.findViewById<TextView>(R.id.itemTV)

    fun bind(item: String) {
        mItemTV.text = item
    }

}